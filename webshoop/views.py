from django.shortcuts import render
from webshoop import models
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from webshoop.forms import TokoForm
from django.urls import reverse_lazy
# Create your views here.

def toko (request):
    if request.method == "POST":
        keyword = request.POST.get('keyword','')
        result = models.Toko.objects.filter(nama_toko = keyword)
        if keyword != '':
            return render (request,'toko.html',{
        'semua_toko' : result,
            })
        else:
            return render (request,'toko.html',{
        'semua_toko' : toko,
            })
        print(keyword)

    return render (request,'toko.html',{
        'semua_toko' : toko
    })

def product (request):
    if request.method == "POST":
        keyword1 = request.POST.get('keyword1','')
        result = models.Produk.objects.filter(nama_produk = keyword1)
        if keyword1 != '':
            return render (request,'produk.html',{
        'semua_product' : result,
            })
        else:
            return render (request,'produk.html',{
        'semua_product' : toko,
            })
        print(keyword1)

    return render (request 'produk.html',{
        'semua_product' : product
    }):

class TokoCreateView(CreateView):
    model = models.Toko
    template_name = 'create_toko.html'
    form_class = TokoForm
    success_url = reverse_lazy('toko')

class TokoUpdateView(UpdateView):
    model = models.Toko
    template_name = 'update_toko.html'
    form_class = TokoForm
    success_url = reverse_lazy('toko')

class TokoDeleteView(DeleteView):
    model = models.Toko
    template_name = 'delete_confirm_toko.html'
    success_url = reverse_lazy('toko')

class ProductCreateView(CreateView):
    model = models.Produk
    template_name = 'create_prodact.html'
    form_class = TokoForm
    success_url = reverse_lazy('product')

class ProductUpdateView(UpdateView):
    model = models.Produk
    template_name = 'update_product.html'
    form_class = TokoForm
    success_url = reverse_lazy('product')

class ProductDeleteView(DeleteView):
    model = models.Produk
    template_name = 'delete_confirm_product.html'
    success_url = reverse_lazy('product')