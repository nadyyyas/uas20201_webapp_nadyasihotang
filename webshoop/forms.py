from django import forms
from webshoop import models

class TokoForm(forms.ModelForm):
    class Meta :
        model = models.Toko
        fields = ('__all__')

    nama_toko = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    nomor_pemilik = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    alamat = forms.CharField(widget=forms.Textarea(attrs={"class":"form-control"}))

class ProdukForm(forms.ModelForm):
    class Meta :
        model = models.Produk
        fields = ('__all__')
    
    nama_produk = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    harga_produk = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    nama_toko = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    keterangan = forms.CharField(widget=forms.Textarea(attrs={"class":"form-control"}))
    keterangan = forms.CharField(widget=forms.RadioSelect(attrs={"class":"form-control"}))