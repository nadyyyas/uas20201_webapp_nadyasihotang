from django.db import models

# Create your models here.
class Toko(models.Model):
    nama_toko = models.CharField(max_length=50)
    nama_pemilik = models.CharField(max_length=50)
    alamat = models.TextField()

    def __str__(self):
        return self.nama_toko

class Produk(models.Model):
    nama_produk = models.CharField(max_length=50)
    harga_produk = models.CharField(max_length=50)
    nama_toko = models.CharField(max_length=50)
    image = models.ImageField
    keterangan = models.TextField()

    def __str__(self):
        return self.nama_toko