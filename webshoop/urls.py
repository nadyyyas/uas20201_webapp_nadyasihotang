from django.urls import include,path
from webshoop import views

urlpatterns = [
    path('', views.shop, name ='toko'),
    path('toko/create/', views.TokoCreateView.as_view(),name ="toko_create"),
    path('toko/update/<int:pk>', views.TokoUpdateView.as_view(),name ="toko_update"),
    path('toko/delete/<int:pk>', views.TokoDeleteView.as_view(),name ="toko_delete"),
    path('produk', views.product),
    path('toko/produk/create/', views.ProductCreateView.as_view(),name ="product_create"),
    path('toko/produk/update/<int:pk>', views.ProductUpdateView.as_view(),name ="product_update"),
    path('toko/produk/delete/<int:pk>', views.ProductDeleteView.as_view(),name ="product_delete"),
]